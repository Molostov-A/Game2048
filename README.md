# 2048WinFormsApp
## Описание проекта
Мой вариант игры 2084 
- Проект создан, на платформе windows forms. 
- Выполнен с соблюдением принципов ООП. 
- Для сохранения результатов используется формат json.
<img src="https://github.com/Molostov-A/Game2048/blob/master/screenshots/Game2048Preview.gif" alt="screenshot" width="800">
